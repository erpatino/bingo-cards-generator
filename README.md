# Bingo cards generator

This is a random bingo cards generator.  
By default it generates 10000 decks of 6 cards each.  
In order to generate a different number of decks, please modify the "for" loop in Application.java
### Constraints
- A deck is composed of 6 cards
- Each card contains 3 x 9 cells
- Each column contains only numbers from its ten group (e.g. 1 to 9 are in the first column, 10 to 19 are in the second column, etc.). Number 90 goes into the 9th column.
- Each card contains  a total of 15 numbers, 5 per row.
- For each card, every column must have at least one cell with an assigned value.
- The numbers in each column are in ascending order.
- A valid deck contains all the numbers from 1 to 90. Each number is present only once.
### Algorithm
For each deck creation:
1. It assigns each of the first 6 numbers of each ten (column group) to a separate card, chosen randomly. At the end of this phase each card contains a value in every column.
2. The remaining numbers are first shuffled (to guarantee an even distribution), then added on a second random iteration across the 6 cards.
3. (Optional if the deck is not complete yet) If there are any incomplete cards and numbers left out, it tries to assign the numbers to these incomplete cards by randomly swapping two pairs of cells between complete and incomplete rows. This will guarantee that the constraint on non-empty columns is satisfied, whilst adding the possibility to assign the numbers that are left out. If there is no space for a given number because the card already contains 3 numbers in the designated column, an exception is thrown and the deck is considered as invalid, as the computational costs to reshuffle the deck is comparable to the creation of a new card deck.
4. For each card, the numbers are then put in ascending order column by column. Empty cells are left in their original position.
6. A rundown of the total successful and unsuccessful deck creation iterations is printed.
7. Once the user presses ENTER, decks are validated and printed.

### Notes
This is a simple, loosely structured Java application. No logging frameworks, as printing the cards would have been impacted.  
No unit tests, as the application, after generating the decks, validates them and prints them. All the constraints are checked at validation time.  
After the generation of the decks, the user is shown some statistics on the execution, along with the calculation time.  
After pressing ENTER in the terminal window, the decks are validated and printed.
