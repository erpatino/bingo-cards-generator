package com.bingo;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        List<CardStrip> decks = new ArrayList<>();
        int generatedDecks = 0;
        int discardedDecks = 0;

        Instant start = Instant.now();
        while(generatedDecks < 10000) {
            CardStrip deck = new BingoGenerator().generateBingoCards();
            if (deck != null) {
                generatedDecks++;
                decks.add(deck);
            } else {
                discardedDecks++;
            }
        }
        Instant end = Instant.now();
        System.out.println(
                String.format("Generated valid decks: %s - Discarded decks: %s - Execution time: %s",
                        decks.size(),
                        discardedDecks,
                        Duration.between(start, end)));
        System.out.println("Press ENTER to validate and print the decks");
        new Scanner(System.in).nextLine();
        for (CardStrip deck : decks) {
            // This first validates the deck, then prints it
            deck.print();
        }
    }

}
