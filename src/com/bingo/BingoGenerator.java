package com.bingo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public class BingoGenerator {

    private List<Card> cards;

    private void init() {
        cards = Arrays.asList(new Card(1), new Card(2), new Card(3), new Card(4), new Card(5),
                new Card(6));
    }

    public CardStrip generateBingoCards() {
        init();
        List<Integer> numbers = getListOfNumbers();
        List<Integer> valuesToBeAssignedAtSecondIteration = new ArrayList<>();
        Deque<Integer> valuesToBeAssignedBySwappingRows = new ArrayDeque<>();
        List<Integer> cardsOrderShuffled = getRandomListOfCardIndexes();

        /* assign first 6 numbers for ten evenly to each card in order to avoid empty columns;
           remaining number to be assigned at a later stage */
        for (int i : numbers) {
            if (i % 10 == 0 || i % 10 > 6) {
                // these numbers will be added at a later stage
                valuesToBeAssignedAtSecondIteration.add(i);
                //reordering cards to spread the distribution evenly
                cardsOrderShuffled = getRandomListOfCardIndexes();
            } else {
                cards.get(cardsOrderShuffled.get(i % 10 - 1)).assignValue(i);
            }
        }

        // shuffling to have a more balanced distribution, otherwise we will always
        // end up with numbers in the late 80s to be part of the third iteration.
        // By doing so we also decrease the chances of a third iteration to be necessary.
        Collections.shuffle(valuesToBeAssignedAtSecondIteration);
        List<Integer> finalCardsOrderShuffled = cardsOrderShuffled;
        valuesToBeAssignedAtSecondIteration.forEach(x -> {
            // if a value cannot be assigned, it will be added to the third iteration (swapping rows between cards)
            if (!assignRandomly(cards, finalCardsOrderShuffled, x)) {
                valuesToBeAssignedBySwappingRows.push(x);
            }
        });

        Deque<Card> incompleteCards = new ArrayDeque<>();
        for (Card card : cards) {
            if (!card.isCardComplete()) {
                incompleteCards.push(card);
            }
        }
        if (!incompleteCards.isEmpty()) {
            try {
                addBySwappingRowData(incompleteCards, valuesToBeAssignedBySwappingRows);
            } catch (RuntimeException e) {
                // An incomplete card does not have space in the designated column. Rebalancing
                // with completed cards is as costly as regenerating another deck
                // due to the lost consistency when it comes to having no empty columns
                return null;
            }
        }
        //Cards are valid, however rows are not sorted yet
        cards.forEach(Card::sortColumns);
        return new CardStrip(cards);
    }

    private boolean assignRandomly(List<Card> cards, List<Integer> cardsOrderShuffled, int i) {
        for (int cardIndex = 0; cardIndex < 6; cardIndex++) {
            if (cards.get(cardsOrderShuffled.get(cardIndex)).assignValue(i)) {
                Collections.shuffle(cardsOrderShuffled);
                return true;
            }
        }
        return false;
    }

    private List<Integer> getListOfNumbers() {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i < 91; i++) {
            numbers.add(i);
        }
        return numbers;
    }

    private List<Integer> getRandomListOfCardIndexes() {
        List<Integer> cardsOrderShuffled = Arrays.asList(0, 1, 2, 3, 4, 5);
        Collections.shuffle(cardsOrderShuffled);
        return cardsOrderShuffled;
    }

    private void addBySwappingRowData(Deque<Card> incompleteCards, Deque<Integer> values) {
        while (!incompleteCards.isEmpty()) {
            int value = values.pop();
            Card incompleteCard = incompleteCards.pop();

            int ten = value == 90 ? 8 : value / 10;
            if (incompleteCard.isColumnFull(ten)) {
                throw new RuntimeException(
                        String.format("Unable to add value %s to card %s", value, incompleteCard.getCardNumber()));
            }
            List<Integer> availableColumnsForSwap = findAvailableColumnsForSwap(incompleteCard, ten);
            // In order to get a random column index, we reshuffle the list of available indexes
            Collections.shuffle(availableColumnsForSwap);
            swapIncompleteCardRowsAndAddValue(incompleteCard, availableColumnsForSwap, ten, value);
            // If not yet complete, re-add to the stack
            if (!incompleteCard.isCardComplete()) {
                incompleteCards.push(incompleteCard);
            }
        }
    }

    private List<Integer> findAvailableColumnsForSwap(Card incompleteCard, int columnToAvoid) {
        List<Integer> availableColumnsForSwap = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            if (i != columnToAvoid && !incompleteCard.isColumnFull(i)) {
                availableColumnsForSwap.add(i);
            }
        }
        return availableColumnsForSwap;
    }

    private void swapIncompleteCardRowsAndAddValue(Card incompleteCard, List<Integer> availableColumnsForSwap,
            int ten, int value) {
        int candidateAvailableRow = 0;
        int candidateFullRow = 0;
        for (int i = 0; i < 3; i++) {
            // if the row is full but there's space in the ten in a full row, we need to swap some data with another row
            if (incompleteCard.getCellData(i, ten) == 0 && incompleteCard.isRowFull(i)) {
                candidateFullRow = i;
            } else if (!incompleteCard.isRowFull(i)) {
                candidateAvailableRow = i;
            }
        }
        for (int j = 0; j < availableColumnsForSwap.size(); j++) {
            int tempColumn = availableColumnsForSwap.get(j);
            // If incomplete row and available row have an assigned value and a zero, swap them
            // then swap the ten as well, then insert the value in the now available row
            if (incompleteCard.getCellData(candidateAvailableRow, tempColumn) == 0
                    && incompleteCard.getCellData(candidateFullRow, tempColumn) != 0) {
                // swap temColumn data between the two rows
                int tempValue = incompleteCard.getCellData(candidateAvailableRow, tempColumn);
                incompleteCard.setCellData(candidateAvailableRow, tempColumn,
                        incompleteCard.getCellData(candidateFullRow, tempColumn));
                incompleteCard.setCellData(candidateFullRow, tempColumn, tempValue);
                // swap ten data between the two rows
                int tempValue2 = incompleteCard.getCellData(candidateAvailableRow, ten);
                incompleteCard.setCellData(candidateAvailableRow, ten,
                        incompleteCard.getCellData(candidateFullRow, ten));
                incompleteCard.setCellData(candidateFullRow, ten, tempValue2);

                // insert the value in the now available row (in the previously full row
                incompleteCard.setCellData(candidateAvailableRow, ten, value);
                break;
            }
        }
    }
}
