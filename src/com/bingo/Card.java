package com.bingo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Card {

    private int cardNumber;
    private int[][] data = new int[3][9];

    public int getCardNumber() {
        return cardNumber;
    }

    public Card(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    public int getCellData(int row, int column) {
        return data[row][column];
    }

    public void setCellData(int row, int column, int value) {
        data[row][column] = value;
    }

    public boolean isColumnFull(int columnNumber) {
        return (data[0][columnNumber] != 0) && (data[1][columnNumber] != 0) && (data[2][columnNumber] != 0);
    }

    public boolean isColumnEmpty(int columnNumber) {
        return (data[0][columnNumber] == 0) && (data[1][columnNumber] == 0) && (data[2][columnNumber] == 0);
    }

    private boolean isColumnSorted(int columnNumber) {
        List<Integer> columnValues = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            if (data[i][columnNumber] != 0) {
                columnValues.add(data[i][columnNumber]);
            }
        }
        return columnValues.stream().sorted().toList().equals(columnValues);
    }

    public boolean isCardComplete() {
        for (int i = 0; i < 3; i++) {
            if (!isRowFull(i)) {
                return false;
            }
        }
        int counter = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                if (data[i][j] != 0) {
                    counter++;
                }
            }
        }
        return counter == 15;
    }

    public boolean isRowFull(int rowNumber) {
        int numberOfSetValues = 0;
        for (int i = 0; i < 9; i++) {
            if (data[rowNumber][i] != 0) {
                numberOfSetValues++;
            }
            if (numberOfSetValues >= 5) {
                return true;
            }
        }
        return false;
    }

    public void print() {
        System.out.println(String.format("------- CARD %s - %s---------", this.cardNumber, isCardComplete() ? "Valid --" : "Invalid "));
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 9; i++) {
                String value = data[j][i] == 0 ? "-" : String.valueOf(data[j][i]);
                System.out.print(value + "\t");
            }
            System.out.println("");
        }
        System.out.println("----------------------------------");
    }

    public boolean contains(int value) {
        int decade = value == 90 ? 8 : value / 10;
        for (int i = 0; i < 3; i++) {
            if (data[i][decade] == value) {
                return true;
            }
        }
        return false;
    }

    public boolean assignValue(int value) {
        if (isCardComplete()) {
            return false;
        }
        int decade = value == 90 ? 8 : value / 10;
        if (isColumnFull(decade)) {
            return false;
        }
        List<Integer> rows = Arrays.asList(0, 1, 2);
        Collections.shuffle(rows);
        for (int randomRow : rows) {
            if (!isRowFull(randomRow)) {
                if (data[randomRow][decade] == 0) {
                    data[randomRow][decade] = value;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isCardValid() {
        for (int column = 0; column < 9; column++) {
            if (isColumnEmpty(column) || !isColumnSorted(column)) {
                return false;
            }
        }
        return isCardComplete();
    }

    public void sortColumns() {
        //for each column
        for (int i = 0; i < 9; i++) {
            //for each row
            for (int j = 0; j < 3; j++) {
                //sort excluding zeros
                for (int k = 0; k < 3; k++) {
                    if (getCellData(j,i) != 0 && getCellData(k,i) > getCellData(j,i)) {
                        int temp = getCellData(k,i);
                        setCellData(k, i, getCellData(j,i));
                        setCellData(j,i,temp);
                    }
                }
            }
        }
    }
}
