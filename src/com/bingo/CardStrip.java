package com.bingo;

import java.util.List;

public class CardStrip {
    private List<Card> cards;

    public CardStrip(List<Card> cards) {
        this.cards = cards;
    }

    private boolean validate() {
        for (Card card : cards) {
            if (!card.isCardValid()) {
                return false;
            }
        }
        for (int i = 1; i< 91; i++) {
            boolean isPresent = false;
            for (Card card : cards) {
                if (card.contains(i)) {
                    isPresent = true;
                    break;
                }
            }
            if (!isPresent) {
                throw new RuntimeException(String.format("The strip does not contain: %s", i));
            }
        }
        return true;
    }

    public void print() {
        System.out.println("\n\n\n----------------------------------");
        System.out.println(String.format("The strip is valid: %s", validate()));
        cards.forEach(Card::print);
        System.out.println("----------------------------------\n\n\n");
    }

}
